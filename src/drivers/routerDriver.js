import {Observable} from 'rx';

export default function (router) {
  return (routes$) => {
    routes$.subscribe(route => {
      router.setRoute(route);
    });
    return Observable.empty();
  };
}
