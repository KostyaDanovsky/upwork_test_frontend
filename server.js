var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var uuid = require('node-uuid');

var plans = [];

app.use(express.static('dist'));
app.use(bodyParser.json())

app.get('/',function(req,res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/getievterritories', function (req, res) {
  var result = territories.filter(function(s) {
    return !s.parent;
  });
  res.send(result);
});

app.get('/territory/:id/subordinates', function (req, res) {
  var parentId = req.params.id;
  var result = territories.filter(function(s) {
    return s.parent && s.parent.id == parentId;
  });
  res.charset = 'utf-8';
  res.json(result);
});

app.post('/new_plan', function (req, res) {
  var newPlan = req.body;
  newPlan.id = uuid.v1();
  var territory = territories.filter(function(t) {
    return t.id == newPlan.territoryId;
  })[0];
  newPlan.territory = territory && territory.name;
  plans[newPlan.id] = newPlan;

  res.charset = 'utf-8';
  res.json(newPlan);
});

app.get('/getplan/:id', function (req, res) {
  var planId = req.params.id;
  var plan = plans[planId];
  res.charset = 'utf-8';
  res.json(plan);
});

app.listen(3000, function () {
  console.log('app is listening on port 3000!');
});

var territories = [
  {
    "id": 1,
    "name": "Bakı şeheri",
    "parent": null
  },
  {
    "id": 2,
    "name": "Sederek rayonu",
    "parent": null
  },
  {
    "id": 3,
    "name": "şerur rayonu",
    "parent": null
  },
  {
    "id": 11,
    "name": "Bineqedi rayonu",
    "parent": {
      "id": 2,
    }
  },
  {
    "id": 12,
    "name": "Qaradağ rayonu",
    "parent": {
      "id": 2,
    }
  },
  {
    "id": 13,
    "name": "Test 1 rayonu",
    "parent": {
      "id": 11,
    }
  },
  {
    "id": 14,
    "name": "Test 2 rayonu",
    "parent": {
      "id": 12,
    }
  },
  {
    "id": 15,
    "name": "Test 3 rayonu",
    "parent": {
      "id": 12,
    }
  }
];