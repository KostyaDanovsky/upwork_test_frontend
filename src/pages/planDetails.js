import {Observable} from 'rx';
import {makeDOMDriver, div, h1, h4} from '@cycle/dom'

const getPlanUrl = 'http://localhost:3000/getplan';

function intent(planId) {
  return Observable.just({
    url: getPlanUrl + `/${planId}`,
    headers: { 'Content-Type': 'application/json; charset=utf-8',  },
  });
}

function model(HTTPSource) {
  return HTTPSource
    .filter(response$ => response$.request.url.indexOf(getPlanUrl) > -1)
    .switch()
    .map(response => response.body)
    .startWith({});
}

function view(plan$) {
  return plan$.map(plan =>
    div([
      h1('Details'),
      h4(`Fərman Adı: ${plan.firstName}`),
      h4(`Fərman Nömresi: ${plan.lastName}`),
      h4(`Başlama tarixi: ${plan.startDate}`),
      h4(`Bitmə tarixi: ${plan.endDate}`),
      h4(`Büdcə: ${plan.budget}`),
      h4(`EV: ${plan.territory}`),
      h4(`Sənəd: ${plan.documents}`),
    ])
  );
}

function details(sources) {
  const request$ = intent(sources.params.planId);
  const planResponse$ = model(sources.HTTP);
  const vtree$ = view(planResponse$);

  return {
    DOM: vtree$,
    HTTP: request$,
  }
}

export default details;
