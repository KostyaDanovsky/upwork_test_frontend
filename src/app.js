import {Observable} from 'rx';
import {run} from '@cycle/core';
import {makeDOMDriver, div, h1, h2, button} from '@cycle/dom';
import {makeHTTPDriver} from '@cycle/http';
import {Router} from 'director';
import routerDriver from './drivers/routerDriver.js';
import main from './pages/main.js';
import details from './pages/planDetails.js';

const router = Router();

const drivers = {
  DOM: makeDOMDriver('#main-container'),
  HTTP: makeHTTPDriver(),
  router: routerDriver(router),
};

function runCycle(component, params = {}) {
  drivers.params = () => params;
  run(component, drivers);
}

const routes = {
  '/': () => runCycle(main),
  '/plan_details/:id': (id) => runCycle(details, {planId: id}),
};

router.mount(routes);
router.init('/');
