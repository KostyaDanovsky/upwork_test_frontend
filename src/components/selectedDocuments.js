import {Observable} from 'rx';
import {div, h3, ul, li} from '@cycle/dom';
import isolate from '@cycle/isolate';

const selectedDocuments = [];

function view(addDocument$) {
  return addDocument$.map(newDocument => {
    if (newDocument && !selectedDocuments.some(existing => existing.id == newDocument.id)) {
      selectedDocuments.push(newDocument);
    }
    return div([
      h3('Selected documents: '),
      ul([
        selectedDocuments.map(d => li(d.name))
      ]),
    ]);
  });
}

function selectedDocumentList(sources) {
  const vtree$ = view(sources.addDocument$);

  return {
    DOM: vtree$,
    selectedDocuments: selectedDocuments,
  };
}

export default function(sources) {
  return isolate(selectedDocumentList)(sources);
}
