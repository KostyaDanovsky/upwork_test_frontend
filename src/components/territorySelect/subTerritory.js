import {Observable} from 'rx';
import {div, select, option} from '@cycle/dom';
import isolate from '@cycle/isolate';

function intent(parentIdChange$) {
  return parentIdChange$.map(pId => {
    return pId
      ? {
        url: `http://localhost:3000/territory/${pId}/subordinates`,
        method: 'GET',
      }
      : {}
  });
}

function model(HTTPSource) {
  return HTTPSource
    .filter(response$ => response$.request.url && response$.request.url.indexOf('subordinates') > -1)
    .switch()
    .map(response => response.body)
    .startWith([]);
}

function view(territoryResponse$) {
  return territoryResponse$.map(territories =>
    div([
      select(territories.map(t => option({value: t.id}, t.name)))
    ])
  );
}

function subTerritorySelect(sources) {
  const request$ = intent(sources.parentIdChange$);
  const territoryResponse$ = model(sources.HTTP);
  const vtree$ = view(territoryResponse$);

  const territoryChange$ = Observable.merge(
    sources.DOM.select('select').events('change').map(e => e.target.value),
    territoryResponse$.map(territories => territories[0] && territories[0].id)
  ).startWith(null);

  return {
    DOM: vtree$,
    HTTP: request$,
    territoryChange$: territoryChange$,
  };
}

export default function(sources) {
  return isolate(subTerritorySelect)(sources);
}
