import {Observable} from 'rx';
import {div, h3, ul, li} from '@cycle/dom';
import isolate from '@cycle/isolate';

const documents = [{id: 1, name: 'Sənəd 1'}, {id: 2, name: 'Sənəd 2'}, {id: 3, name: 'Sənəd 3'},
  {id: 4, name: 'Sənəd 4'}, {id: 5, name: 'Sənəd 5'}, {id: 6, name: 'Sənəd 6'}];

function intent(DOMSource) {
  return DOMSource.select('li')
    .events('click')
    .map(event => {
      const id = event.target.id.split('document-')[1];
      return documents.filter(d => d.id == id)[0];
    })
    .startWith(null);
}

function view(addDocumentsAction$) {
  return addDocumentsAction$.map(clicked =>
    !clicked
      ? div()
      : div([
          h3('List of documents:'),
          ul([
            documents.map(d => li({id: `document-${d.id}`}, d.name))
          ]),
        ])
  );
}

function documentList(sources) {
  const addDocument$ = intent(sources.DOM);
  const vtree$ = view(sources.addDocumentsAction$);

  return {
    DOM: vtree$,
    addDocument$: addDocument$,
  };
}

export default function (sources) {
  return isolate(documentList)(sources);
}
