import {Observable} from 'rx';
import {div} from '@cycle/dom';
import territory from './territory.js';
import subTerritory from './subTerritory.js';

function territories(sources) {
  const territorySinks = territory(sources);
  const subTerritoryParentSinks = subTerritory({
    DOM: sources.DOM,
    HTTP: sources.HTTP,
    parentIdChange$: territorySinks.territoryChange$,
  });
  const subTerritoryChildSinks = subTerritory({
    DOM: sources.DOM,
    HTTP: sources.HTTP,
    parentIdChange$: subTerritoryParentSinks.territoryChange$,
  });

  const vtree$ = Observable.combineLatest(territorySinks.DOM, subTerritoryParentSinks.DOM, subTerritoryChildSinks.DOM,
    (territoryVTree, subTerritoryParentVTree, subTerritoryChildVTree) => {
      return div([
        territoryVTree,
        subTerritoryParentVTree,
        subTerritoryChildVTree,
      ]);
    });

  const httpSinks = Observable.merge(territorySinks.HTTP, subTerritoryParentSinks.HTTP, subTerritoryChildSinks.HTTP);

  return {
    DOM: vtree$,
    HTTP: httpSinks,
    territory$: subTerritoryChildSinks.territoryChange$,
  };
}

export default territories;
