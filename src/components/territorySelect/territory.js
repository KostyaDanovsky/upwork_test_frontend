import {Observable} from 'rx';
import {div, select, option} from '@cycle/dom';
import isolate from '@cycle/isolate';

const getTerritoriesUrl = 'http://localhost:3000/getievterritories';

function intent() {
  return Observable.of({
    url: getTerritoriesUrl,
    headers: { 'Content-Type': 'application/json; charset=utf-8',  },
  });
}

function model(HTTPSource) {
  return HTTPSource
    .filter(response$ => response$.request.url === getTerritoriesUrl)
    .switch()
    .map(response => response.body)
    .startWith([]);
}

function view(territoryResponse$) {
  return territoryResponse$.map(territories =>
    div([select(territories.map(t => option({value: t.id}, t.name)))])
  );
}

function territorySelect(sources) {
  const request$ = intent();
  const territoryResponse$ = model(sources.HTTP);
  const vtree$ = view(territoryResponse$);

  const territoryChange$ = Observable.merge(
    sources.DOM.select('select').events('change').map(e => e.target.value),
    territoryResponse$.map(territories => territories[0] && territories[0].id)
  ).startWith(null);

  return {
    DOM: vtree$,
    HTTP: request$,
    territoryChange$: territoryChange$,
  };
}

export default function(sources) {
  return isolate(territorySelect)(sources);
}
