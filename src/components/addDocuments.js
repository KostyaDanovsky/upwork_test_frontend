import {Observable} from 'rx';
import {div, button} from '@cycle/dom';
import isolate from '@cycle/isolate';

function intent(DOMSource) {
  return DOMSource.select('button').events('click').map(e => e.target).startWith(null);
}

function view() {
  return Observable.of(
    div([
      button('Sened elave et')
    ])
  );
}

function addDocuments(sources) {
  const addDocumentsAction$ = intent(sources.DOM);
  const vtree$ = view();

  return {
    DOM: vtree$,
    addDocumentsAction$: addDocumentsAction$,
  };
}

export default function (sources) {
  return isolate(addDocuments)(sources);
}
