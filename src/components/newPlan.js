import {Observable} from 'rx';
import {div, button, span, label, input, br} from '@cycle/dom';
import isolate from '@cycle/isolate';
import selectedDocuments from './selectedDocuments.js';

const newPlanUrl = 'http://localhost:3000/new_plan';

function intent(DOMSource, selectedDocuments, territory$) {

  function getInputStream(className) {
    return DOMSource.select(className).events('input').map(event => event.target.value);
  }

  const firstName$ = getInputStream('.first-name');
  const lastName$ = getInputStream('.last-name');
  const startDate$ = getInputStream('.start-date');
  const endDate$ = getInputStream('.end-date');
  const budget$ = getInputStream('.budget');

  const formSubmissionEvent$ = DOMSource.select('button.new-plan').events('click');

  const formSubmissionData$ = Observable.combineLatest(firstName$, lastName$, startDate$, endDate$, budget$, territory$,
    (firstName, lastName, startDate, endDate, budget, territory) => {
      return {
        firstName: firstName,
        lastName: lastName,
        startDate: startDate,
        endDate: endDate,
        budget: budget,
        territoryId: territory,
        documents: selectedDocuments.map(d => d.name).join('; '),
      };
    }).sample(formSubmissionEvent$);

  return formSubmissionData$.map(data => {
      return {
        url: newPlanUrl,
        method: 'POST',
        send: data,
        type: 'application/json'
      }
    });
}

function model(HTTPSource) {
  return HTTPSource
    .filter(response$ => response$.request.url === newPlanUrl)
    .switch()
    .map(response => response.body)
    .startWith(null);
}

function view(selectedDocumentsVTree) {
  return Observable.just(
    div([
      selectedDocumentsVTree,
      div([ label('Fərman Adı'), input('.first-name') ]),
      div([ label('Fərman Nömresi'), input('.last-name') ]),
      div([ label('Başlama tarixi'), input('.start-date') ]),
      div([ label('Bitmə tarixi'), input('.end-date') ]),
      div([ label('Büdcə'), input('.budget') ]),
      br(),
      button('.new-plan', 'Gönder'),
    ])
  );
}

function newPlan(sources) {
  const selectedDocumentsSinks = selectedDocuments({
    DOM: sources.DOM,
    addDocument$: sources.addDocument$,
  });

  const request$ = intent(sources.DOM, selectedDocumentsSinks.selectedDocuments, sources.territory$);
  const planResponse$ = model(sources.HTTP);
  const vtree$ = view(selectedDocumentsSinks.DOM);

  const route$ = planResponse$.map(newPlan => {
    return newPlan ? `/plan_details/${newPlan.id}`: '/';
  });

  return {
    DOM: vtree$,
    HTTP: request$,
    router: route$,
  };
}

export default function(sources) {
  return isolate(newPlan)(sources);
}
