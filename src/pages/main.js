import {Observable} from 'rx';
import {br, div} from '@cycle/dom';
import territories from '../components/territorySelect/territories.js';
import addDocuments from '../components/addDocuments.js';
import documents from '../components/documents.js';
import newPlan from '../components/newPlan.js';

function main(sources) {
  const territoriesSinks = territories(sources);
  const addDocumentsSinks = addDocuments(sources);
  const documentsSinks = documents({
    DOM: sources.DOM,
    addDocumentsAction$: addDocumentsSinks.addDocumentsAction$,
  });
  const newPlanSinks = newPlan({
    DOM: sources.DOM,
    HTTP: sources.HTTP,
    territory$: territoriesSinks.territory$,
    addDocument$: documentsSinks.addDocument$,
  });

  let vtree$ = Observable.combineLatest(territoriesSinks.DOM, addDocumentsSinks.DOM, documentsSinks.DOM, newPlanSinks.DOM,
    (territoriesVTree, addDocumentsVTree, documentsVTree, newPlanVTree) => {
      return div([
        territoriesVTree,
        br(),
        addDocumentsVTree,
        documentsVTree,
        newPlanVTree,
      ]);
    });

  return {
    DOM: vtree$,
    HTTP: Observable.merge(territoriesSinks.HTTP, newPlanSinks.HTTP),
    router: newPlanSinks.router,
  };
}

export default main;